import React,{useState} from 'react';
import { Link } from 'react-router-dom';


const NuevaCuenta = () => {

    const [usuario,guardarUsuario] = useState({
        nombre:'',
        email:'',
        password:'',
        confirmar:''
    })

    const {email,password} = usuario;


    const onchange =e=> {

        guardarUsuario({
            ...usuario,
            [e.target.name]:e.target.value
        })

    }

    //Cuando el usuario desee iniciar sesión

    const onSubmit =e=>{
        e.preventDefault();

        //Validar que no haya campos vacios

        //Envio de los datos
    }

    return(
    <div className="form-usuario">
        <div className="contenedor-form sombra-dark">
            <h1>Obtener una Cuenta</h1>


            <form onSubmit={onSubmit}>
                <div className="campo-form">
                    <label htmlFor="nombre">Nombre:</label>
                    <input 
                        type ="text" 
                        id = "nombre"
                        name = "nombre"
                        placeholder = "Tu nombre"
                        value={email}
                        onchange ={onchange}
                    
                    />
                </div>

                <div className="campo-form">
                    <label htmlFor="email">Email:</label>
                    <input 
                        type ="email" 
                        id = "email"
                        name = "email"
                        placeholder = "Tu email"
                        value={email}
                        onchange ={onchange}
                    
                    />
                </div>

                <div className="campo-form">
                    <label htmlFor="password">Password:</label>
                    <input 
                        type ="password" 
                        id = "password"
                        name = "password"
                        placeholder = "Tu password"
                        value={password}
                        onchange ={onchange}
                    
                    />
                </div>

                <div className="campo-form">
                    <label htmlFor="confirmar">Confirmar Password:</label>
                    <input 
                        type ="password" 
                        id = "confirmar"
                        name = "confirmar"
                        placeholder = "Repite tu password"
                        value={password}
                        onchange ={onchange}
                    
                    />
                </div>

                <div className="campo-form">
                    <input type="submit" className="btn btn-primario btn-block"
                        value="Registrar"
                    
                    />
                </div>

            </form>
            
            <Link to={"/nueva-cuenta"} className="enlace-cuenta">
            Obtener Cuenta
            </Link>

        </div>
    </div>
    )

}

export default NuevaCuenta;