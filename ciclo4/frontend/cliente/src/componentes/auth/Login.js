import React,{useState} from 'react';
import { Link } from 'react-router-dom';


const Login = () => {

    const [usuario,guardarUsuario] = useState({
        email:'',
        password:''
    })

    const {email,password} = usuario;


    const onchange =e=> {

        guardarUsuario({
            ...usuario,
            [e.target.name]:e.target.value
        })

    }

    //Cuando el usuario desee iniciar sesión

    const onSubmit =e=>{
        e.preventDefault();

        //Validar que no haya campos vacios

        //Envio de los datos
    }

    return(

    <div >

<div id="banner">
    <div id="banner-content">
   Ejempli
    </div>
  </div>
 
 <div className="titulo">Desayunos Online</div>

    <div className="form-usuario">        

        <div className="contenedor-form sombra-dark">
            <h1>Iniciar Sesión</h1>


            <form onSubmit={onSubmit}>
                <div className="campo-form">
                    <label htmlFor="email">Prueba de clone:</label>
                    <input 
                        type ="email" 
                        id = "email"
                        name = "email"
                        placeholder = "Tu email"
                        value={email}
                        onchange ={onchange}
                    
                    />
                </div>

                <div className="campo-form">
                    <label htmlFor="password">Password:</label>
                    <input 
                        type ="password" 
                        id = "password"
                        name = "password"
                        placeholder = "Tu password"
                        value={password}
                        onchange ={onchange}
                    
                    />
                </div>

                <div className="campo-form">
                    <input type="submit" className="btn btn-primario btn-block"
                        value="Iniciar Sesión"
                    
                    />
                </div>

            </form>
            
            <Link to={"/nueva-cuenta"} className="enlace-cuenta">
            Obtener Cuenta
            </Link>

        </div> 
    </div>

    </div>    
    )

}


export default Login;
